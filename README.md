# Super simple function to lookup a PID and Kill it 

```
import commands
import subprocess as sub

def killPID(fileName):
    pidRaw = str(commands.getstatusoutput('ps axf | grep '+str(fileName)+''))
    pidRaw = pidRaw.replace("(0, '", "")
    pidRaw = pidRaw.replace("\\\\_", "")
    pidRaw = pidRaw.replace("\\n", "$")
    pidRaw = pidRaw.split("$")
    for pidRawRow in pidRaw:
        pidRawRow = pidRawRow.replace("'", "")
        pidRawRow = pidRawRow.replace('"', '')
        pidRawRow = pidRawRow.split(' ')
        cleanPID = pidRawRow[0]
        killNit = sub.Popen(['kill', str(cleanPID)], stdout=sub.PIPE,stderr=sub.PIPE)
        output, errors = killNit.communicate()

if __name__ == '__main__':
    killPID(<<SOME PATH AND FILE NAME>>)
	
```